package Client;

import Common.Order;
import Common.Product;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.ArrayList;

public class ClientView {

    /**
     * FRAME
     */
    private JFrame clientFrame;

    /**
     * PANELS
     */
    private JPanel clientPanel;
    private JPanel connectPanel;
    private JPanel logInPanel;
    private JPanel createAccountPanel;
    private JPanel diningPanel;
    private JPanel paymentPanel;
    private JPanel categoryPanel;
    private JPanel comboPanel;
    private JPanel snackPanel;
    private JPanel beveragePanel;
    private JPanel seasonalPanel;
    private JPanel searchPanel;
    private JPanel cartPanel;
    private JPanel editPanel;
    private JPanel receiptPanel;
    private JPanel historyPanel;

    /**
     * TEXTFIELDS
     */
    private JTextField username; // done
    private JPasswordField password; // done
    private JTextField enterUsername; // done
    private JPasswordField enterPassword; // done
    private JPasswordField reEnterPasswordField; // done
    private JTextField emailField; // done
    private JTextField firstNameField; // done
    private JTextField lastNameField; // done
    private JTextField searchField;
    private JTextField searchQuantityField;
    private JTextField comboQuantityField; // done
    private JTextField snackQuantityField;
    private JTextField beverageQuantityField;
    private JTextField seasonalQuantityField;
    private JTextField editQuantityField;

    /**
     * BUTTONS
     */
    private JButton connectButton;
    private JButton logInButton; // done
    private JButton createAccountButton; // done
    private JButton createButton; // done
    private JButton dineInButton; // done
    private JButton takeOutButton; // done
    private JButton cashButton; // done
    private JButton cardButton; // done
    private JButton comboButton; // done
    private JButton snackButton; // done
    private JButton beverageButton; // done
    private JButton seasonalButton; // done
    private JButton comboAddButton; // done
    private JButton snackAddButton; // done
    private JButton beverageAddButton; // done
    private JButton seasonalAddButton; // done
    private JButton searchButton;
    private JButton searchAddButton;
    private JButton historyButton;
    private JButton cartButton;
    private JButton editButton;
    private JButton saveEditButton;
    private JButton checkOutButton;
    private JButton exitButton;
    private JButton backInDining;
    private JButton backInPayment;
    private JButton backInCategoryButton;
    private JButton backInComboListButton;
    private JButton backInSnackListButton;
    private JButton backInBeverageListButton;
    private JButton backInSeasonalListButton;
    private JButton backInSearchButton;
    private JButton backInCartButton;
    private JButton backInEdit;
    private JButton backInHistory;
    private JButton backCreateButton;

    /**
     * TABLES
     */
    private JTable comboListTable; // done
    private JTable snackListTable;
    private JTable beverageListTable;
    private JTable cartTable;
    private JTable searchTable;
    private JTable receiptTable;
    private JTable seasonalListTable;
    private JTable editCartTable;
    private JTable historyTable;

    /**
     * LABELS
     */
    private JLabel invalidUsername; // done
    private JLabel invalidPassword; // done
    private JLabel logInError; // done
    private JLabel comboQuantityError;
    private JLabel snackQuantityError;
    private JLabel beverageQuantityError;
    private JLabel seasonalQuantityError;
    private JLabel editQuantityError;
    private JLabel enterPasswordCreate;
    private JLabel enterUsernameCreate;
    private JLabel firstNameCreate;
    private JLabel lastNameCreate;
    private JLabel reEnterPasswordCreate;
    private JLabel emailCreate;
    private JLabel invalidEmailClientJLabel;
    private JLabel existingUsernameClientJLabel;
    private JLabel invalidReenterPassClientJLabel;
    private JLabel comboPanelAccept;
    private JLabel snackPanelAccept;
    private JLabel beveragePanelAccept;
    private JLabel seasonalPanelAccept;
    private JLabel searchPanelAccept;
    private JLabel editPanelAccept;
    private JLabel createPanelAccept;
    private JButton orderAgainButton;
    private JButton logOutInCategory;
    private JTextArea historyTexrArea;
    private JScrollPane historyJscrollpane;
    private ClientModel clientModel;

    public ClientView(ClientModel clientModel){
        this.clientModel = clientModel;
    }

    /**
     * clientPanel
     */
    public JPanel getClientPanel() {
        return clientPanel;
    }

    /**
     * connectPanel
     */
    public void showConnect() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "connectPanel");
    }
    public JButton getConnectButton() {
        return connectButton;
    }

    /**
     * logInPanel
     */
    public String getUsernameClientTextField(){
        return username.getText();
    }
    public JTextField setUsernameClientTextField(){
        return username;
    }
    public String getPasswordClientTextField(){
        return password.getText();
    }
    public JTextField setPasswordClientTextField(){
        return password;
    }
    public JButton getLogInButton(){
        return logInButton;
    }
    public JButton getCreateAccountButton(){
        return createAccountButton;
    }
    public void setUsernameError(String message) {
        invalidUsername.setText(message);
    }
    public void setPasswordError(String message) {
        invalidPassword.setText(message);
    }
    public void setLogInError(String message) {
        logInError.setText(message);
    }
    public void showLogIn() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "logInPanel");
    }

    /**
     * createAccount
     *
     */
    public JButton getCreateButton() {
        return createButton;
    }
    public String getEnterUsername() {
        return enterUsername.getText();
    }
    public String getEnterPassword() {
        return enterPassword.getText();
    }
    public String getEmailField() {
        return emailField.getText();
    }
    public String getFirstNameField() {
        return firstNameField.getText();
    }
    public String getLastNameField() {
        return lastNameField.getText();
    }
    public String getReEnterPasswordField() {
        return reEnterPasswordField.getText();
    }
    public JTextField setEnterUsername() {
        return enterUsername;
    }
    public JPasswordField setEnterPassword() {
        return enterPassword;
    }
    public JTextField setEmailField() {
        return emailField;
    }
    public JTextField setFirstNameField() {
        return firstNameField;
    }
    public JTextField setLastNameField() {
        return lastNameField;
    }
    public JPasswordField setReEnterPasswordField() {
        return reEnterPasswordField;
    }
    public void setEmailCreateError(String message) {
        invalidEmailClientJLabel.setText(message);
    };
    public void setUsernameCreateError(String message) {
        existingUsernameClientJLabel.setText(message);
    };
    public void setPasswordCreate(String message) {
        invalidReenterPassClientJLabel.setText(message);
    };
    public JButton getBackCreateButton() {
        return backCreateButton;
    }
    public void setCreateAccountPanel(String message){
        createPanelAccept.setText(message);
    }
    public void showCreateAccount(){
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "createAccount");
    }

    /**
     * diningPanel
     */
    public JButton getDineInButton(){
        return dineInButton;
    }
    public JButton getTakeOutButton(){
        return takeOutButton;
    }
    public JButton getBackInDining(){
        return backInDining;
    }
    public void showDining(){
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel,"dining");
    }

    /**
     * paymentPanel
     */
    public JButton getCashButton(){
        return cashButton;
    }
    public JButton getCardButton(){
        return cardButton;
    }
    public JButton getBackInPayment(){
        return backInPayment;
    }
    public void showPayment() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "payment");
    }

    /**
     * categoryPanel
     */
    public JButton getComboButton(){
        return comboButton;
    }
    public JButton getSnackButton(){
        return snackButton;
    }
    public JButton getBeverageButton(){
        return beverageButton;
    }
    public JButton getSeasonalButton(){
        return seasonalButton;
    }
    public JButton getBackInCategory(){
        return backInCategoryButton;
    }
    public String getSearchInCategory() {
        return searchField.getText();
    }
    public JButton getSearchButton(){
        return searchButton;
    }
    public JButton getHistoryButton(){
        return historyButton;
    }
    public JButton getCartButton(){
        return cartButton;
    }
    public JButton getLogOutInCategory(){return logOutInCategory;}
    public void showCategory() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "category");
    }

    /**
     * comboPanel
     */
    public JTable getComboListTable() {
        return comboListTable;
    }
    public void comboListTable(ArrayList<Product> productsCombo) {

        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCK"};
        DefaultTableModel model = new DefaultTableModel(productsCombo.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        getComboListTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getComboListTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < productsCombo.size(); i++) {
            getComboListTable().setValueAt(productsCombo.get(i).getName(), i + 1, 0);
            getComboListTable().setValueAt(productsCombo.get(i).getDesc(), i + 1, 1);
            getComboListTable().setValueAt(productsCombo.get(i).getPrice(), i + 1, 2);
            getComboListTable().setValueAt(productsCombo.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel comboColumnModel = getComboListTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        comboColumnModel.getColumn(0).setCellRenderer(center);
        comboColumnModel.getColumn(1).setCellRenderer(center);
        comboColumnModel.getColumn(1).setPreferredWidth(400);
        comboColumnModel.getColumn(2).setCellRenderer(center);
        comboColumnModel.getColumn(3).setCellRenderer(center);
        getComboListTable().setColumnModel(comboColumnModel);
    }
    public JButton getComboAddButton(){
        return comboAddButton;
    }
    public JTextField setComboQuantityField() {
        return comboQuantityField;
    }
    public String getComboQuantity() {
        return comboQuantityField.getText();
    }
    public void setComboQuantityError(String message) {
        if (message.equalsIgnoreCase("")) {
            comboQuantityError.setText("");
        } else {
            comboQuantityError.setText(message);
        }
    }
    public JButton getBackInComboListButton(){
        return backInComboListButton;
    }
    public void showComboList() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "comboList");
    }
    public void setComboPanelAccept(String message){
        comboPanelAccept.setText(message);
    }

    /**
     * snackPanel
     */
    public JTable getSnackListTable() {
        return snackListTable;
    }
    public void snackListTable(ArrayList<Product> productsSnacks) {
        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCKS"};
        DefaultTableModel model = new DefaultTableModel(productsSnacks.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getSnackListTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getSnackListTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < productsSnacks.size(); i++) {
            getSnackListTable().setValueAt(productsSnacks.get(i).getName(), i + 1, 0);
            getSnackListTable().setValueAt(productsSnacks.get(i).getDesc(), i + 1, 1);
            getSnackListTable().setValueAt(productsSnacks.get(i).getPrice(), i + 1, 2);
            getSnackListTable().setValueAt(productsSnacks.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel snackColumnModel = getSnackListTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        snackColumnModel.getColumn(0).setCellRenderer(center);
        snackColumnModel.getColumn(0).setPreferredWidth(200);
        snackColumnModel.getColumn(1).setCellRenderer(center);
        snackColumnModel.getColumn(1).setPreferredWidth(200);
        snackColumnModel.getColumn(2).setCellRenderer(center);
        snackColumnModel.getColumn(3).setCellRenderer(center);
        getSnackListTable().setColumnModel(snackColumnModel);
    }
    public JButton getSnackAddButton(){
        return snackAddButton;
    }
    public String getSnackQuantity() {
        return snackQuantityField.getText();
    }
    public JTextField setSnackQuantityField() {
        return snackQuantityField;
    }
    public void setSnackQuantityError(String message) {
        if (message.equalsIgnoreCase("")) {
            snackQuantityError.setText("");
        } else {
            snackQuantityError.setText(message);
        }
    }
    public JButton getBackInSnackListButton(){
        return backInSnackListButton;
    }
    public void showSnackList() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "snackList");
    }
    public void setSnackPanelAccept(String message){
        snackPanelAccept.setText(message);
    }

    /**
     * beveragePanel
     */
    public JTable getBeverageListTable() {
        return beverageListTable;
    }
    public void beverageListTable(ArrayList<Product> productsBeverage) {
        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCKS"};
        DefaultTableModel model = new DefaultTableModel(productsBeverage.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getBeverageListTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getBeverageListTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < productsBeverage.size(); i++) {
            getBeverageListTable().setValueAt(productsBeverage.get(i).getName(), i + 1, 0);
            getBeverageListTable().setValueAt(productsBeverage.get(i).getDesc(), i + 1, 1);
            getBeverageListTable().setValueAt(productsBeverage.get(i).getPrice(), i + 1, 2);
            getBeverageListTable().setValueAt(productsBeverage.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel beverageColumnModel = getBeverageListTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        beverageColumnModel.getColumn(0).setCellRenderer(center);
        beverageColumnModel.getColumn(0).setPreferredWidth(200);
        beverageColumnModel.getColumn(1).setCellRenderer(center);
        beverageColumnModel.getColumn(1).setPreferredWidth(400);
        beverageColumnModel.getColumn(2).setCellRenderer(center);
        beverageColumnModel.getColumn(3).setCellRenderer(center);
        getBeverageListTable().setColumnModel(beverageColumnModel);
    }
    public JButton getBeverageAddButton(){
        return beverageAddButton;
    }
    public String getBeverageQuantity() {
        return beverageQuantityField.getText();
    }
    public JTextField setBeverageQuantityField() {
        return beverageQuantityField;
    }
    public void setBeverageQuantityError(String message) {
        if (message.equalsIgnoreCase("")) {
            beverageQuantityError.setText("");
        } else {
            beverageQuantityError.setText(message);
        }
    }
    public JButton getBackInBeverageListButton(){
        return backInBeverageListButton;
    }
    public void showBeverageList() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "beverageList");
    }

    public void setBeveragePanelAccept(String message) {
        beveragePanelAccept.setText(message);
    }

    /**
     * seasonalPanel
     */
    public JTable getSeasonalListTable() {
        return seasonalListTable;
    }
    public void seasonalListTable(ArrayList<Product> productsSeasonal) {
        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCKS"};
        DefaultTableModel model = new DefaultTableModel(productsSeasonal.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getSeasonalListTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getSeasonalListTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < productsSeasonal.size(); i++) {
            getSeasonalListTable().setValueAt(productsSeasonal.get(i).getName(), i + 1, 0);
            getSeasonalListTable().setValueAt(productsSeasonal.get(i).getDesc(), i + 1, 1);
            getSeasonalListTable().setValueAt(productsSeasonal.get(i).getPrice(), i + 1, 2);
            getSeasonalListTable().setValueAt(productsSeasonal.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel seasonalColumnModel = getSeasonalListTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        seasonalColumnModel.getColumn(0).setCellRenderer(center);
        seasonalColumnModel.getColumn(0).setPreferredWidth(100);
        seasonalColumnModel.getColumn(1).setCellRenderer(center);
        seasonalColumnModel.getColumn(1).setPreferredWidth(200);
        seasonalColumnModel.getColumn(2).setCellRenderer(center);
        seasonalColumnModel.getColumn(3).setCellRenderer(center);
        getSeasonalListTable().setColumnModel(seasonalColumnModel);
    }
    public JButton getSeasonalAddButton(){
        return seasonalAddButton;
    }
    public String getSeasonalQuantity() {
        return seasonalQuantityField.getText();
    }
    public JTextField setSeasonalQuantityField() {
        return seasonalQuantityField;
    }
    public void setSeasonalQuantityError(String message) {
        if (message.equalsIgnoreCase("")) {
            seasonalQuantityError.setText("");
        } else {
            seasonalQuantityError.setText(message);
        }
    }
    public JButton getBackInSeasonalListButton(){
        return backInSeasonalListButton;
    }
    public void showSeasonalList() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "seasonalList");
    }
    public void setSeasonalPanelAccept(String message){
        seasonalPanelAccept.setText(message);
    }
    /**
     * searchPanel
     */
    public JTable getSearchTable() {
        return searchTable;
    }
    public void searchListTable(ArrayList<Product> result) {
        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCKS"};
        DefaultTableModel model = new DefaultTableModel(result.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getSearchTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getSearchTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < result.size(); i++) {
            getSearchTable().setValueAt(result.get(i).getName(), i + 1, 0 );
            getSearchTable().setValueAt(result.get(i).getDesc(), i + 1, 1);
            getSearchTable().setValueAt(result.get(i).getPrice(), i + 1, 2);
            getSearchTable().setValueAt(result.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel resultColumnModel = getSearchTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        resultColumnModel.getColumn(0).setCellRenderer(center);
        resultColumnModel.getColumn(1).setCellRenderer(center);
        resultColumnModel.getColumn(1).setPreferredWidth(400);
        resultColumnModel.getColumn(2).setCellRenderer(center);
        resultColumnModel.getColumn(3).setCellRenderer(center);
        getSearchTable().setColumnModel(resultColumnModel);
    }
    public JButton getSearchAddButton(){
        return searchAddButton;
    }
    public String getQuantitySearchField() {
        return searchQuantityField.getText();
    }
    public JTextField setQuantitySearchField() {
        return searchQuantityField;
    }
    public void setSearchError(String message) {
        searchField.setText(message);
    }
    public JButton getBackInSearchButton(){
        return backInSearchButton;
    }
    public void setSearchPanelAccept(String message){
        searchPanelAccept.setText(message);
    }
    public void showSearch() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "search");
    }

    /**
     * cartPanel
     */
    public JTable getCartListTable() {
        return cartTable;
    }
    public void cartListTable(ArrayList<Order> cart) {
        String[] header = {"NAME", "PRICE", "QUANTITY"};
        DefaultTableModel model = new DefaultTableModel(cart.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getCartListTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getCartListTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < cart.size(); i++) {
            getCartListTable().setValueAt(cart.get(i).getName(), i + 1, 0);
            getCartListTable().setValueAt(cart.get(i).getPrice(), i + 1, 1);
            getCartListTable().setValueAt(cart.get(i).getQuantity(), i + 1, 2);
        }
        TableColumnModel cartColumnModel = getCartListTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        cartColumnModel.getColumn(0).setCellRenderer(center);
        cartColumnModel.getColumn(1).setCellRenderer(center);
        cartColumnModel.getColumn(2).setCellRenderer(center);
        getCartListTable().setColumnModel(cartColumnModel);
    }

    public JButton getCheckOutButton(){
        return checkOutButton;
    }
    public JButton getEditButton(){
        return editButton;
    }
    public JButton getBackInCartButton(){
        return backInCartButton;
    }
    public void showCart() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "cart");
    }

    /**
     * editPanel
     */
    public JTable getEditCartTable() {
        return editCartTable;
    }
    public void editListTable(ArrayList<Order> cart) {

        String[] header = {"NAME", "PRICE", "QUANTITY"};
        DefaultTableModel model = new DefaultTableModel(cart.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getEditCartTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getEditCartTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < cart.size(); i++) {
            getEditCartTable().setValueAt(cart.get(i).getName(), i + 1, 0);
            getEditCartTable().setValueAt(cart.get(i).getPrice(), i + 1, 1);
            getEditCartTable().setValueAt(cart.get(i).getQuantity(), i + 1, 2);
        }
        TableColumnModel cartColumnModel = getEditCartTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        cartColumnModel.getColumn(0).setCellRenderer(center);
        cartColumnModel.getColumn(1).setCellRenderer(center);
        cartColumnModel.getColumn(2).setCellRenderer(center);
        getEditCartTable().setColumnModel(cartColumnModel);
    }
    public int getEditIndex() {
        int[] selectedRow = new int[1];
        getEditCartTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                selectedRow[0] = getComboListTable().getSelectedRow();
            }
        });
        return selectedRow[0];
    }
    public JButton getSaveEditButton() {
        return saveEditButton;
    }
    public String getEditQuantityTextField() {
        return editQuantityField.getText();
    }
    public JTextField setEditQuantityTextField() {
        return editQuantityField;
    }
    public void setEditQuantityError(String message) {
        editQuantityError.setText(message);
    }
    public JButton getBackInEdit(){
        return backInEdit;
    }
    public void showEdit() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "edit");
    }
    public void setEditPanelAccept(String message){
        editPanelAccept.setText(message);
    }

    /**
     * receiptPanel
     */
    public JTable getReceiptTable() {
        return receiptTable;
    }
    public void receiptTable(String username, String diningOption, String paymentOption, double totalPrice, ArrayList<Order> cart) {
        String[] header = {"NAME", "DINE", "PAYMENT", "TOTAL PRICE", "ORDER/S", "PRICE", "QUANTITY"};
        DefaultTableModel model = new DefaultTableModel(cart.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getReceiptTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getReceiptTable().setValueAt(header[i], 0, i);
        }
        getReceiptTable().setValueAt(username, 1, 0);
        getReceiptTable().setValueAt(diningOption, 1, 1);
        getReceiptTable().setValueAt(paymentOption, 1, 2);
        getReceiptTable().setValueAt(totalPrice, 1, 3);
        for (int i = 0; i < cart.size(); i++) {
            getReceiptTable().setValueAt(cart.get(i).getName(), i + 1, 4);
            getReceiptTable().setValueAt(cart.get(i).getPrice(), i + 1, 5);
            getReceiptTable().setValueAt(cart.get(i).getQuantity(), i + 1, 6);
        }
        TableColumnModel cartColumnModel = getReceiptTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        cartColumnModel.getColumn(0).setCellRenderer(center);
        cartColumnModel.getColumn(1).setCellRenderer(center);
        cartColumnModel.getColumn(2).setCellRenderer(center);
        cartColumnModel.getColumn(3).setCellRenderer(center);
        cartColumnModel.getColumn(3).setPreferredWidth(100);
        cartColumnModel.getColumn(4).setCellRenderer(center);
        cartColumnModel.getColumn(4).setPreferredWidth(100);
        cartColumnModel.getColumn(5).setCellRenderer(center);
        cartColumnModel.getColumn(6).setCellRenderer(center);
        getReceiptTable().setColumnModel(cartColumnModel);
    }
    public JButton getExitButton(){
        return exitButton;
    }
    public JButton getOrderAgainButton(){
        return orderAgainButton;
    }
    public void showReceipt() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "receipt");
    }

    /**
     * historyPanel
     */
    public JTextArea getHistoryTexrArea() {
        return historyTexrArea;
    }
    public JScrollPane getHistoryJscrollpane(){ return historyJscrollpane;
    }
    public JButton getHistoryBackButton(){
        return backInHistory;
    }
    public void showHistory() {
        CardLayout cardLayout = (CardLayout) clientPanel.getLayout();
        cardLayout.show(clientPanel, "customerHistory");
    }
}

