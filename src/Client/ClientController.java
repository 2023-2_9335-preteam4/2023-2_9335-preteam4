package Client;

import Common.Order;
import Common.Product;
import Common.ClientHandlerInterface;
import Common.Receipt;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class ClientController extends JFrame {
    private final ClientView clientView;
    private final ClientModel clientModel;
    public static ClientHandlerInterface stub;
    public ArrayList<Product> productsCombo = new ArrayList<>();
    public ArrayList<Product> productsSnack = new ArrayList<>();
    public ArrayList<Product> productsBeverage = new ArrayList<>();
    public ArrayList<Product> productsSeasonal = new ArrayList<>();
    public ArrayList<Product> searchResult = new ArrayList<>();
    public ArrayList<Order> cart = new ArrayList<>();
    public String username, diningOption, paymentOption, quantity;
    public int selectedRow;
    public double totalPrice;

    public ClientController() {
        this.clientModel = new ClientModel();
        this.clientView =new ClientView(clientModel);

        add(clientView.getClientPanel());
        setTitle("NYIRPS");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1000,700);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
        setResizable(false);

        clientView.getConnectButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    stub = clientModel.connectClientRMI("localhost");
                    clientModel.addObserver(clientView);
                    stub.addMessage(username + " connected to the server!");
                    stub.addOnlineUser(username);
                    clientView.showLogIn();
                } catch (IOException | ClassNotFoundException | NotBoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getCreateAccountButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientView.setUsernameClientTextField().setText("");
                clientView.setPasswordClientTextField().setText("");
                clientView.setLogInError("");
                clientView.setUsernameError("");
                clientView.setPasswordError("");
                clientView.showCreateAccount();
            }
        });

        clientView.getCreateButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String firstName = clientView.getFirstNameField();
                String lastName = clientView.getLastNameField();
                String createUsername = clientView.getEnterUsername();
                String createPassword = clientView.getEnterPassword();
                String email = clientView.getEmailField();
                String rePassword = clientView.getReEnterPasswordField();
                String isCreated;
                try {
                    if (createUsername.isBlank()
                            || createPassword.isBlank()
                            || firstName.isBlank()
                            || lastName.isBlank()
                            || email.isBlank()
                            || rePassword.isBlank()) {
                        clientView.setEmailCreateError("Please fill all the fields!");
                        clientView.setUsernameCreateError("");
                        clientView.setPasswordCreate("");
                        clientView.setCreateAccountPanel("");
                    } else {
                        isCreated = stub.createAccount(firstName, lastName, createUsername, createPassword, email, rePassword, "customers");
                        if (isCreated.equalsIgnoreCase("email")) {
                            clientView.setEmailCreateError("Email already existing!");
                            clientView.setUsernameCreateError("");
                            clientView.setPasswordCreate("");
                            clientView.setCreateAccountPanel("");
                        }
                        if (isCreated.equalsIgnoreCase("username")) {
                            clientView.setEmailCreateError("");
                            clientView.setUsernameCreateError("Username already existing!");
                            clientView.setPasswordCreate("");
                            clientView.setCreateAccountPanel("");
                        }
                        if (isCreated.equalsIgnoreCase("password")) {
                            clientView.setEmailCreateError("");
                            clientView.setUsernameCreateError("");
                            clientView.setPasswordCreate("Password do not match!");
                            clientView.setCreateAccountPanel("");
                        }
                        if (isCreated.equalsIgnoreCase("created")){
                            clientView.setCreateAccountPanel("Created Successfully!");
                            clientView.setEmailCreateError("");
                            clientView.setUsernameCreateError("");
                            clientView.setPasswordCreate("");
                            clientView.setFirstNameField().setText("");
                            clientView.setLastNameField().setText("");
                            clientView.setEnterUsername().setText("");
                            clientView.setEnterPassword().setText("");
                            clientView.setEmailField().setText("");
                            clientView.setReEnterPasswordField().setText("");
                        }
                    }
                } catch (RemoteException | FileNotFoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getBackCreateButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientView.setEmailCreateError("");
                clientView.setUsernameCreateError("");
                clientView.setPasswordCreate("");
                clientView.setFirstNameField().setText("");
                clientView.setLastNameField().setText("");
                clientView.setEnterUsername().setText("");
                clientView.setEnterPassword().setText("");
                clientView.setEmailField().setText("");
                clientView.setReEnterPasswordField().setText("");
                clientView.showLogIn();
            }
        });

        clientView.getLogInButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                username = clientView.getUsernameClientTextField();
                String password = clientView.getPasswordClientTextField();
                String isLoggedIn;

                try {
                    if (username.isBlank() || password.isBlank()) {
                        clientView.setLogInError("Please fill all the fields!");
                        clientView.setUsernameError("");
                        clientView.setPasswordError("");
                    } else {
                        isLoggedIn = stub.validateAccount(username, password, "customers");
                        if (isLoggedIn.equalsIgnoreCase("username")) {
                            clientView.setLogInError("");
                            clientView.setUsernameError("Username is not found!");
                            clientView.setPasswordError("");
                        } else if (isLoggedIn.equalsIgnoreCase("password")) {
                            clientView.setLogInError("");
                            clientView.setUsernameError("");
                            clientView.setPasswordError("Incorrect password!");
                        } else {
                            clientView.setUsernameClientTextField().setText("");
                            clientView.setPasswordClientTextField().setText("");
                            stub.addMessage(username + " connected to the server.");
                            stub.addOnlineUser(username);
                            setTitle("Client " + username);
                            clientView.showDining();
                        }
                    }
                } catch (Exception a) {
                    a.printStackTrace();
                }
            }
        });

        clientView.getDineInButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                diningOption = "Dine-in";
                clientView.showPayment();
            }
        });

        clientView.getTakeOutButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                diningOption = "Take-out";
                clientView.showPayment();
            }
        });
        clientView.getCashButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                paymentOption = "Cash";
                clientView.showCategory();
            }
        });

        clientView.getCardButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                paymentOption = "Card";
                clientView.showCategory();
            }
        });

        clientView.getBackInPayment().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.showDining();
            }
        });
        clientView.getLogOutInCategory().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    stub.addMessage(username + " disconnected to the server.");
                    stub.removeUser(username);
                } catch (RemoteException ex) {
                    throw new RuntimeException(ex);
                }
                clientView.setUsernameClientTextField().setText("");
                clientView.setPasswordClientTextField().setText("");
                clientView.setLogInError("");
                clientView.setUsernameError("");
                clientView.setPasswordError("");
                diningOption = null;
                paymentOption = null;
                cart.clear();
                clientView.showLogIn();
                setTitle("NYIRPS");
            }
        });

        clientView.getComboButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    productsCombo = stub.extractProductsJSON("CM");
                    clientView.comboListTable(productsCombo);
                    clientView.showComboList();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getComboListTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setComboPanelAccept("");
                selectedRow = clientView.getComboListTable().getSelectedRow();
            }
        });

        clientView.getComboAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String quantity = clientView.getComboQuantity();

                if (quantity.isBlank()) {
                    clientView.setComboQuantityError("Please fill the quantity field!");
                    clientView.setComboPanelAccept("");
                    clientView.setComboQuantityField().setText("");
                }
                if (selectedRow < 0) {
                    clientView.setComboQuantityError("Please select a product!");
                }

                for (int i = 0; i < productsCombo.size(); i++) {
                    if (i == selectedRow - 1) {
                        if (Integer.parseInt(quantity) > productsCombo.get(i).getStocks()) {
                            clientView.setComboQuantityError("Too low on stocks right now!");
                        } else {
                            cart.add(new Order(productsCombo.get(i).getProductCode(),
                                    productsCombo.get(i).getName(),
                                    productsCombo.get(i).getDesc(),
                                    productsCombo.get(i).getPrice(),
                                    Integer.parseInt(quantity)));
                            int newStocks = productsCombo.get(i).getStocks() - Integer.parseInt(quantity);
                            try {
                                if (productsCombo.get(i).getStocks() <= 0) {
                                    clientView.setComboQuantityError("Product " + i + 1 + " is not available!");
                                }
                                stub.updateProductStock(productsCombo.get(i).getName(), newStocks);
                                clientView.setComboPanelAccept("Added to cart!");
                                clientView.setComboQuantityField().setText("");
                                clientView.setComboQuantityError("");
                            } catch (IOException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }
            }
        });

        clientView.getBackInComboListButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.setComboPanelAccept("");
                clientView.setComboQuantityField().setText("");
                clientView.setComboQuantityError("");
                clientView.showCategory();
            }
        });

        clientView.getSnackButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    productsSnack = stub.extractProductsJSON("SNM");
                    clientView.snackListTable(productsSnack);
                    clientView.showSnackList();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getSnackListTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setSnackPanelAccept("");
                selectedRow = clientView.getSnackListTable().getSelectedRow();
            }
        });

        clientView.getSnackAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String quantity = clientView.getSnackQuantity();

                if (quantity.isBlank()) {
                    clientView.setSnackQuantityError("Please fill the quantity field!");
                    clientView.setSnackPanelAccept("");
                    clientView.setSnackQuantityField().setText("");
                }
                if (selectedRow < 0) {
                    clientView.setSnackQuantityError("Please select a product!");
                }

                for (int i = 0; i < productsSnack.size(); i++) {
                    if (i == selectedRow - 1) {
                        if (Integer.parseInt(quantity) > productsSnack.get(i).getStocks()) {
                            clientView.setSnackQuantityError("Too low on stocks right now!");
                        } else {
                            cart.add(new Order(productsSnack.get(i).getProductCode(),
                                    productsSnack.get(i).getName(),
                                    productsSnack.get(i).getDesc(),
                                    productsSnack.get(i).getPrice(),
                                    Integer.parseInt(quantity)));
                            int newStocks = productsSnack.get(i).getStocks() - Integer.parseInt(quantity);
                            try {
                                if (productsSnack.get(i).getStocks() <= 0) {
                                    clientView.setSnackQuantityError("Product " + i + 1 + " is not available!");
                                }
                                stub.updateProductStock(productsSnack.get(i).getName(), newStocks);
                                clientView.setSnackPanelAccept("Added to cart!");
                                clientView.setSnackQuantityField().setText("");
                                clientView.setSnackQuantityError("");
                            } catch (IOException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }
            }
        });

        clientView.getBackInSnackListButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clientView.setSnackPanelAccept("");
                clientView.setSnackQuantityField().setText("");
                clientView.setSnackQuantityError("");
                clientView.showCategory();
            }
        });

        clientView.getBeverageButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    productsBeverage = stub.extractProductsJSON("BM");
                    clientView.beverageListTable(productsBeverage);
                    clientView.showBeverageList();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getBeverageListTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setBeveragePanelAccept("");
                selectedRow = clientView.getBeverageListTable().getSelectedRow();
            }
        });

        clientView.getBeverageAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String quantity = clientView.getBeverageQuantity();

                if (quantity.isBlank()) {
                    clientView.setBeverageQuantityError("Please fill the quantity field!");
                    clientView.setBeveragePanelAccept("");
                    clientView.setBeverageQuantityField().setText("");
                }
                if (selectedRow < 0) {
                    clientView.setSnackQuantityError("Please select a product!");
                }

                for (int i = 0; i < productsBeverage.size(); i++) {
                    if (i == selectedRow - 1) {
                        if (Integer.parseInt(quantity) > productsBeverage.get(i).getStocks()) {
                            clientView.setBeverageQuantityError("Too low on stocks right now!");
                        } else {
                            cart.add(new Order(productsBeverage.get(i).getProductCode(),
                                    productsBeverage.get(i).getName(),
                                    productsBeverage.get(i).getDesc(),
                                    productsBeverage.get(i).getPrice(),
                                    Integer.parseInt(quantity)));
                            int newStocks = productsBeverage.get(i).getStocks() - Integer.parseInt(quantity);
                            try {
                                if (productsBeverage.get(i).getStocks() <= 0) {
                                    clientView.setBeverageQuantityError("Product " + i + 1 + " is not available!");
                                }
                                stub.updateProductStock(productsBeverage.get(i).getName(), newStocks);
                                clientView.setBeveragePanelAccept("Added to cart!");
                                clientView.setBeverageQuantityField().setText("");
                                clientView.setBeverageQuantityError("");
                            } catch (IOException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }
            }
        });

        clientView.getBackInBeverageListButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.setBeveragePanelAccept("");
                clientView.setBeverageQuantityField().setText("");
                clientView.setBeverageQuantityError("");
                clientView.showCategory();
            }
        });

        clientView.getSeasonalButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    productsSeasonal = stub.extractProductsJSON("SSM");
                    clientView.seasonalListTable(productsSeasonal);
                    clientView.showSeasonalList();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getSeasonalListTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setSeasonalPanelAccept("");
                selectedRow = clientView.getSeasonalListTable().getSelectedRow();
            }
        });

        clientView.getSeasonalAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String quantity = clientView.getSeasonalQuantity();

                if (quantity.isBlank()) {
                    clientView.setSeasonalQuantityError("Please fill the quantity field!");
                    clientView.setSeasonalPanelAccept("");
                    clientView.setSeasonalQuantityField().setText("");
                }
                if (selectedRow < 0) {
                    clientView.setSeasonalQuantityError("Please select a product!");
                }

                for (int i = 0; i < productsSeasonal.size(); i++) {
                    if (i == selectedRow - 1) {
                        if (Integer.parseInt(quantity) > productsSeasonal.get(i).getStocks()) {
                            clientView.setSeasonalQuantityError("Too low on stocks right now!");
                        } else {
                            cart.add(new Order(productsSeasonal.get(i).getProductCode(),
                                    productsSeasonal.get(i).getName(),
                                    productsSeasonal.get(i).getDesc(),
                                    productsSeasonal.get(i).getPrice(),
                                    Integer.parseInt(quantity)));
                            int newStocks = productsSeasonal.get(i).getStocks() - Integer.parseInt(quantity);
                            try {
                                if (productsSeasonal.get(i).getStocks() <= 0) {
                                    clientView.setSeasonalQuantityError("Product " + i + 1 + " is not available!");
                                }
                                stub.updateProductStock(productsSeasonal.get(i).getName(), newStocks);
                                clientView.setSeasonalPanelAccept("Added to cart!");
                                clientView.setSeasonalQuantityField().setText("");
                                clientView.setSeasonalQuantityError("");
                            } catch (IOException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }
            }
        });

        clientView.getBackInSeasonalListButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.setBeveragePanelAccept("");
                clientView.setSeasonalQuantityField().setText("");
                clientView.setSeasonalQuantityError("");
                clientView.showCategory();
            }
        });

        clientView.getSearchButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String searchKey = clientView.getSearchInCategory();
                if (searchKey.isBlank()) {
                    clientView.setSearchError("Please fill the search field!");
                } else {
                    try {
                        searchResult = stub.searchProductJSON(searchKey);
                        if (searchResult.isEmpty()) {
                            clientView.setSearchError("Product is not on the list!");
                        } else {
                            clientView.searchListTable(searchResult);
                            clientView.setSearchError("");
                            clientView.showSearch();
                        }
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }
        });

        clientView.getSearchTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setSearchPanelAccept("");
                selectedRow = clientView.getSearchTable().getSelectedRow();
            }
        });

        clientView.getSearchAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String quantity = clientView.getQuantitySearchField();

                if (quantity.isBlank()) {
                    clientView.setSearchPanelAccept("Please fill the quantity field!");
                    clientView.setSeasonalQuantityField().setText("");
                }

                if (selectedRow < 0) {
                    clientView.setSearchPanelAccept("Please select a product!");
                }

                for (int i = 0; i < searchResult.size(); i++) {
                    if (i == selectedRow - 1) {
                        if (Integer.parseInt(quantity) > searchResult.get(i).getStocks()) {
                            clientView.setSearchPanelAccept("Too low on stocks right now!");
                        } else {
                            cart.add(new Order(searchResult.get(i).getProductCode(),
                                    searchResult.get(i).getName(),
                                    searchResult.get(i).getDesc(),
                                    searchResult.get(i).getPrice(),
                                    Integer.parseInt(quantity)));
                            int newStocks = searchResult.get(i).getStocks() - Integer.parseInt(quantity);
                            try {
                                if (searchResult.get(i).getStocks() <= 0) {
                                    clientView.setSearchPanelAccept("Product " + i + 1 + " is not available!");
                                }
                                stub.updateProductStock(searchResult.get(i).getName(), newStocks);
                                clientView.setSearchPanelAccept("Added to cart!");
                                clientView.setQuantitySearchField().setText("");
                                clientView.setSearchError("");
                            } catch (IOException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }
            }
        });

        clientView.getBackInSearchButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.setSearchPanelAccept("");
                clientView.setQuantitySearchField().setText("");
                clientView.setSearchError("");
                clientView.showCategory();
            }
        });

        clientView.getHistoryButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Receipt> history = stub.showCustomerHistory(username);
                    StringBuilder historyText = new StringBuilder();
                    for (Receipt receipt : history) {
                        historyText.append(receipt.toString()).append("\n");
                    }
                    clientView.getHistoryTexrArea().setText(historyText.toString());
                    clientView.getHistoryJscrollpane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                    clientView.showHistory();
                } catch (RemoteException | FileNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });

        clientView.getHistoryBackButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clientView.showCategory();
            }
        });

        clientView.getCartButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.cartListTable(cart);
                clientView.showCart();
            }
        });

        clientView.getBackInCartButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.showCategory();
            }
        });

        clientView.getEditButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clientView.editListTable(cart);
                clientView.showEdit();
            }
        });

        clientView.getEditCartTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                clientView.setEditPanelAccept("");
                selectedRow = clientView.getEditCartTable().getSelectedRow();
            }
        });

        clientView.getBackInEdit().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.setEditPanelAccept("");
                clientView.setEditQuantityError("");
                clientView.setEditQuantityTextField().setText("");
                clientView.showCategory();
            }
        });

        clientView.getSaveEditButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                quantity = clientView.getEditQuantityTextField();

                if (quantity.isBlank()) {
                    clientView.setEditQuantityError("Please fill the quantity field!");
                    clientView.setEditPanelAccept("");
                    clientView.setEditQuantityTextField().setText("");
                }
                if (selectedRow < 0) {
                    clientView.setEditQuantityError("Please select a product!");
                }

                for (int i = 0; i < cart.size(); i++) {
                    if (i == selectedRow - 1) {
                        cart.get(i).setQuantity(Integer.parseInt(quantity));
                        if (cart.get(i).getQuantity() == 0) {
                            cart.remove(cart.get(i));
                            clientView.setEditPanelAccept("Edited successfully!");
                            clientView.setEditQuantityError("");
                            clientView.setEditQuantityTextField().setText("");
                        }
                    }
                }
            }
        });

        clientView.getCheckOutButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                try {
                    totalPrice = 0.0;
                    for (Order i : cart) {
                        totalPrice += (i.getPrice() * i.getQuantity());
                    }
                    clientView.receiptTable(username, diningOption, paymentOption, totalPrice, cart);
                    stub.receiptWriterJSON(username, diningOption, paymentOption, totalPrice, cart);
                    clientView.showReceipt();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        clientView.getExitButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    stub.addMessage(username + " disconnected to the server.");
                    stub.removeUser(username);
                } catch (RemoteException ex) {
                    throw new RuntimeException(ex);
                }
                diningOption = null;
                paymentOption = null;
                cart.clear();
                clientView.showLogIn();
                setTitle("NYIRPS");
            }
        });
        clientView.getOrderAgainButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                diningOption = null;
                paymentOption = null;
                cart.clear();
                clientView.showDining();
            }
        });

        clientView.getBackInCategory().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                clientView.showPayment();
            }
        });
    }
}
