package Client;

import Common.ClientHandlerInterface;
import Common.Order;
import Common.Product;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ClientModel {
    private ArrayList<String> messages;
    private ClientView clientView;
    private static Registry registry;
    public static ClientHandlerInterface stub;

    public ClientModel(){
        this.messages = new ArrayList<>();
    }

    public ClientHandlerInterface connectClientRMI(String serverIPAddress) throws IOException, ClassNotFoundException, NotBoundException {
        try {
            registry = LocateRegistry.getRegistry(serverIPAddress, Registry.REGISTRY_PORT);
            Remote remoteObj = registry.lookup("ServerModel");
            if (remoteObj instanceof ClientHandlerInterface) {
                stub = (ClientHandlerInterface) remoteObj;
                return stub;
            } else {
                throw new RuntimeException("Remote Object does not implement ClientHandlerInterface");
            }
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize RMI connection", e);
        }
    }

    public void disconnectClientRMI() {
        try {
            UnicastRemoteObject.unexportObject(stub, true);
            System.out.println("Disconnected from server.");
        } catch (RemoteException e) {
            System.err.println("Error disconnecting from server: " + e.getMessage());
        }
    }

    public void addObserver(ClientView clientView) {
        this.clientView = clientView;
    }
}
