package Server;

import javax.swing.*;
import java.awt.*;

public class ServerView extends Component {
    private JPanel serverPanel;
    private JTextArea connectionTextarea;
    private JTextArea onlineTextarea;
    private JButton startServerbtn;
    private JButton stopServerbtn;
    private ServerModel serverModel;
    public ServerView(ServerModel serverModel){
        this.serverModel = serverModel;
    }
    public JPanel getServerPanel(){
        return serverPanel;
    }
    public JButton getStopServerbtn (){
        stopServerbtn.setEnabled(false);
        return stopServerbtn;
    }
    public JButton getStartServerbtn (){
        return startServerbtn;
    }

    public JTextArea getConnectionTextarea() {
        return connectionTextarea;
    }

    public JTextArea getOnlineTextArea() {
        return onlineTextarea;
    }

    public void updateUsers(){
        for (String user : serverModel.getOnlineUsers()){
            onlineTextarea.append(user + "\n");
        }
    }

    public void updateMessage() {
        SwingUtilities.invokeLater(() -> {
            String latestMessage = serverModel.getLatestMessage();
            connectionTextarea.append(latestMessage);
        });
    }
}


