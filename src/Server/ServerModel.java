package Server;

import Common.ClientHandlerInterface;
import Common.Order;
import Common.Product;
import Common.Receipt;
import com.google.gson.*;
import java.io.*;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ServerModel extends UnicastRemoteObject implements ClientHandlerInterface {
    private final ArrayList<String> onlineUsers;
    private final ArrayList<String> messages;
    private ServerView serverView;
    private Registry registry;

    protected ServerModel() throws RemoteException {
        super();
        messages = new ArrayList<>();
        onlineUsers = new ArrayList<>();
        serverView = new ServerView(this);
    }

    public void startServerRMI() {
        try {
            ClientHandlerInterface stub = new ServerModel();
            registry = LocateRegistry.createRegistry(1099);
            registry.rebind("ServerModel", stub);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void stopServerRMI() {
        try {
            registry.unbind("ServerModel");
            UnicastRemoteObject.unexportObject(registry, true);
        } catch (RemoteException | NotBoundException e) {
            System.err.println("Error stopping server: " + e);
        }
    }

    public String createAccount(String firstName, String lastName, String username, String password, String email, String reEnterPassword, String user) {

        try {
            JsonParser parser = new JsonParser();
            FileReader reader = new FileReader("res/accounts.json");
            JsonObject jsonData = parser.parse(reader).getAsJsonObject();
            JsonArray accounts = jsonData.getAsJsonArray(user);

            for (int i = 0; i < accounts.size(); i++) {
                JsonObject account = accounts.get(i).getAsJsonObject();
                if (account.get("username").getAsString().equals(username)) {
                    return "username";
                }
                if (account.get("email").getAsString().equals(email)) {
                    return "email";
                }
                if(!password.equals(reEnterPassword)) {
                    return "password";
                }
            }

            JsonObject newAccount = new JsonObject();
            newAccount.addProperty("first_name", firstName);
            newAccount.addProperty("last_name", lastName);
            newAccount.addProperty("username", username);
            newAccount.addProperty("password", password);
            newAccount.addProperty("email", email);
            accounts.add(newAccount);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            FileWriter writer = new FileWriter("res/accounts.json");
            gson.toJson(jsonData, writer);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "created";
    }

    public String validateAccount(String username, String password, String user) {
        try {
            JsonParser parser = new JsonParser();
            FileReader reader = new FileReader("res/accounts.json");
            JsonElement jsonData = parser.parse(reader);
            JsonArray accounts = jsonData.getAsJsonObject().getAsJsonArray(user);
            for (JsonElement accountElement : accounts) {
                JsonObject account = accountElement.getAsJsonObject();
                String storedUsername = account.get("username").getAsString();
                String storedPassword = account.get("password").getAsString();
                if (storedUsername.equals(username)) {
                    if (storedPassword.equals(password)) {
                        return "loggedIn";
                    } else {
                        return "password";
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "username";
    }

    public ArrayList<Product> extractProductsJSON(String productCode) {

        ArrayList<Product> productList = new ArrayList<>();

        try (FileReader reader = new FileReader("res/products.json")) {
            JsonParser parser = new JsonParser();
            JsonElement jsonData = parser.parse(reader);
            JsonArray productArray = jsonData.getAsJsonObject().getAsJsonArray("products");

            for (JsonElement productElement : productArray) {
                JsonObject productObject = productElement.getAsJsonObject();
                String code = productObject.get("productCode").getAsString();

                if (productCode.equals(code)) {
                    String name = productObject.get("name").getAsString();
                    String desc = productObject.get("desc").getAsString();
                    int price = productObject.get("price").getAsInt();
                    int stock = productObject.get("stock").getAsInt();
                    productList.add(new Product(code, name, desc, price, stock));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productList;
    }

    public ArrayList<Product> searchProductJSON(String searchKey) throws RemoteException, FileNotFoundException {

        ArrayList<Product> combo = extractProductsJSON("CM");
        ArrayList<Product> snack = extractProductsJSON("SNM");
        ArrayList<Product> beverage = extractProductsJSON("BM");
        ArrayList<Product> seasonal = extractProductsJSON("SSM");
        ArrayList<Product> result = new ArrayList<>();
        for (Product i : combo) {
            if (searchKey.equalsIgnoreCase(i.getName())) {
                result.add(i);
            }
        }
        for (Product i : snack) {
            if (searchKey.equalsIgnoreCase(i.getName())) {
                result.add(i);
            }
        }
        for (Product i : beverage) {
            if (searchKey.equalsIgnoreCase(i.getName())) {
                result.add(i);
            }
        }
        for (Product i : seasonal) {
            if (searchKey.equalsIgnoreCase(i.getName())) {
                result.add(i);
            }
        }
        return result;
    }

    public ArrayList<Product> showAllProductsJSON() throws RemoteException, FileNotFoundException {

        ArrayList<Product> combo = extractProductsJSON("CM");
        ArrayList<Product> snack = extractProductsJSON("SNM");
        ArrayList<Product> beverage = extractProductsJSON("BM");
        ArrayList<Product> seasonal = extractProductsJSON("SSM");
        ArrayList<Product> result = new ArrayList<>();
        for (Product i : combo) {
            result.add(i);
        }
        for (Product i : snack) {
            result.add(i);
        }
        for (Product i : beverage) {
            result.add(i);
        }
        for (Product i : seasonal) {
            result.add(i);
        }
        return result;
    }

    public void receiptWriterJSON(String name, String diningOption, String paymentOption, double totalPrice, ArrayList<Order> cart) {
        try {
            JsonObject jsonData;
            try (FileReader fileReader = new FileReader("res/receipt.json")) {
                JsonParser jsonParser = new JsonParser();
                jsonData = jsonParser.parse(fileReader).getAsJsonObject();
            }

            ZoneId phZone = ZoneId.of("Asia/Manila");
            LocalDate date = LocalDate.now();
            LocalTime time = LocalTime.now(phZone);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
            String phTime = time.format(formatter);


            JsonObject customerObj = new JsonObject();
            customerObj.addProperty("name", name);
            customerObj.addProperty("diningOption", diningOption);
            customerObj.addProperty("paymentOption", paymentOption);
            customerObj.addProperty("totalPrice", totalPrice);
            customerObj.addProperty("order", cart.toString());
            customerObj.addProperty("date", date.toString());
            customerObj.addProperty("time", phTime);

            JsonArray customersArray = jsonData.getAsJsonArray("receipts");

            customersArray.add(customerObj);

            try (FileWriter fileWriter = new FileWriter("res/receipt.json")) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                gson.toJson(jsonData, fileWriter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String createProduct(String Id, String name, String description,double price, int stock) {


        String result;

        try {
            JsonParser parser = new JsonParser();
            FileReader reader = new FileReader("res/products.json");
            JsonObject jsonData = parser.parse(reader).getAsJsonObject();
            JsonArray products = jsonData.getAsJsonArray("products");

            JsonObject newAccount = new JsonObject();
            newAccount.addProperty("productCode", Id);
            newAccount.addProperty("name", name);
            newAccount.addProperty("desc", description);
            newAccount.addProperty("price", price);
            newAccount.addProperty("stock", stock);
            products.add(newAccount);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            FileWriter writer = new FileWriter("res/products.json");
            gson.toJson(jsonData, writer);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result = "created";
    }

    public void writeProductsToJsonFile(ArrayList<Product> products) throws RemoteException, FileNotFoundException {

        String filename = "res/products.json";

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        for (Product product : products) {
            JsonObject jsonProduct = new JsonObject();
            jsonProduct.addProperty("productCode", product.getProductCode());
            jsonProduct.addProperty("name", product.getName());
            jsonProduct.addProperty("desc", product.getDesc());
            jsonProduct.addProperty("price", product.getPrice());
            jsonProduct.addProperty("stock", product.getStocks());
            jsonArray.add(jsonProduct);
        }

        jsonObject.add("products", jsonArray);

        try (FileWriter writer = new FileWriter(filename)) {
            gson.toJson(jsonObject, writer);
        } catch (IOException e) {
            System.err.println("Failed to write to the file: " + e.getMessage());
        }
    }

    public void updateProductStock(String productName, int newStock) throws RemoteException, IOException {
        // Read JSON file contents
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(new FileReader("res/products.json")).getAsJsonObject();

        // Get the "products" array
        JsonArray products = jsonObject.getAsJsonArray("products");

        // Find the product by name and update its stock
        for (int i = 0; i < products.size(); i++) {
            JsonObject product = products.get(i).getAsJsonObject();
            if (product.get("name").getAsString().equals(productName)) {
                product.addProperty("stock", newStock);
                break;
            }
        }

        // Write the updated JSON content back to the file
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter("res/products.json")) {
            gson.toJson(jsonObject, writer);
        } catch (IOException e) {
            System.err.println("Failed to write to the file: " + e.getMessage());
        }
    }

    public ArrayList<Receipt> showCustomerHistory(String name) throws FileNotFoundException {

        ArrayList<Receipt> receipt = new ArrayList<>();

        Gson gson = new Gson();
        try (Reader reader = new FileReader("res/receipt.json")) {
            JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);
            JsonArray receipts = jsonObject.getAsJsonArray("receipts");

            for (JsonElement receiptElement : receipts) {
                JsonObject receiptObject = receiptElement.getAsJsonObject();
                String receiptName = receiptObject.get("name").getAsString();

                if (receiptName.equals(name)) {
                    receipt.add(new Receipt(receiptName,
                            receiptObject.get("diningOption").getAsString(),
                            receiptObject.get("paymentOption").getAsString(),
                            receiptObject.get("totalPrice").getAsDouble(),
                            receiptObject.get("order").getAsString(),
                            receiptObject.get("date").getAsString(),
                            receiptObject.get("time").getAsString()));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return receipt;
    }

    public ArrayList<Receipt> showAllReceipts() throws FileNotFoundException {

        ArrayList<Receipt> allReceipts = new ArrayList<>();

        Gson gson = new Gson();
        try (Reader reader = new FileReader("res/receipt.json")) {
            JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);
            JsonArray receipts = jsonObject.getAsJsonArray("receipts");

            for (JsonElement receiptElement : receipts) {
                JsonObject receiptObject = receiptElement.getAsJsonObject();

                System.out.println(receiptObject.get("totalPrice").getAsDouble());

                allReceipts.add(new Receipt(
                        receiptObject.get("name").getAsString(),
                        receiptObject.get("diningOption").getAsString(),
                        receiptObject.get("paymentOption").getAsString(),
                        receiptObject.get("totalPrice").getAsDouble(),
                        receiptObject.get("order").getAsString(),
                        receiptObject.get("date").getAsString(),
                        receiptObject.get("time").getAsString()));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return allReceipts;
    }

    public void addOnlineUser(String name) {
        onlineUsers.add(name);
        serverView.updateUsers();
    }

    public void removeUser(String name){
        onlineUsers.remove(name);
        serverView.updateUsers();
    }

    public ArrayList<String> getOnlineUsers() {
        return onlineUsers;
    }

    public void addMessage(String s) {
        messages.add(s + "\n");
        serverView.updateMessage();
    }

    public String getLatestMessage(){
        return messages.get(messages.size()-1);
    }

    public void addObserver(ServerView serverView){
        this.serverView = serverView;
    }
}