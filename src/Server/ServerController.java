package Server;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.RemoteException;

public class ServerController extends JFrame {
    private final ServerModel serverModel;
    private final ServerView serverView;
    public ServerController() throws RemoteException {

        this.serverModel = new ServerModel();
        this.serverView = new ServerView(serverModel);
        this.serverModel.addObserver(serverView);

        add(serverView.getServerPanel());
        setTitle("SERVER");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setSize(500,500);
        setLocationRelativeTo(null);
        setVisible(true);

        serverView.getStartServerbtn().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                serverModel.startServerRMI();
                serverModel.addMessage("Server is running... Waiting for clients.");
                serverView.getStopServerbtn().setEnabled(true);
                serverView.getStartServerbtn().setEnabled(false);

            }

        });
        serverView.getStopServerbtn().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                serverModel.addMessage("Server is closed.");
                serverModel.stopServerRMI();
                serverView.getStartServerbtn().setEnabled(true);
                serverView.getStopServerbtn().setEnabled(false);
            }
        });
    }
}
