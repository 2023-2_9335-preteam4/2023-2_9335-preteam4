package Test;

import Common.Product;
import Common.Receipt;
import com.google.gson.*;

import java.io.*;
import java.rmi.RemoteException;
import java.util.ArrayList;


public class Test {
    public static void main(String[] args) throws IOException {
        updateProductStock("res/products.json", "Combo A", 1);
    }

    public static void updateProductStock(String filePath, String productName, int newStock) throws IOException {
        // Read JSON file contents
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(new FileReader(filePath)).getAsJsonObject();

        // Get the "products" array
        JsonArray products = jsonObject.getAsJsonArray("products");

        // Find the product by name and update its stock
        for (int i = 0; i < products.size(); i++) {
            JsonObject product = products.get(i).getAsJsonObject();
            if (product.get("name").getAsString().equals(productName)) {
                product.addProperty("stock", newStock);
                break;
            }
        }

        // Write the updated JSON content back to the file
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter(filePath)) {
            gson.toJson(jsonObject, writer);
        } catch (IOException e) {
            System.err.println("Failed to write to the file: " + e.getMessage());
        }
    }

}
