package Common;

import Server.ServerView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ClientHandlerInterface extends Remote {
    void startServerRMI() throws RemoteException;
    void stopServerRMI() throws RemoteException;
    void updateProductStock(String productName, int newStock) throws RemoteException, IOException;
    String createAccount(String firstName, String lastName, String username, String password, String email, String reEnterPassword, String user) throws RemoteException, FileNotFoundException;
    String validateAccount(String username, String password, String users) throws RemoteException, FileNotFoundException;
    ArrayList<Product> extractProductsJSON(String category) throws RemoteException, FileNotFoundException;
    ArrayList<Product> searchProductJSON(String searchKey) throws RemoteException, FileNotFoundException;
    ArrayList<Product> showAllProductsJSON() throws RemoteException, FileNotFoundException;
    ArrayList<Receipt> showCustomerHistory(String name) throws RemoteException, FileNotFoundException;
    void receiptWriterJSON(String name, String diningOption, String paymentOption, double totalPrice, ArrayList<Order> cart) throws RemoteException;
    void addOnlineUser(String name) throws RemoteException;
    void removeUser(String name) throws RemoteException;
    ArrayList<String> getOnlineUsers() throws RemoteException;
    void addMessage(String s) throws RemoteException;
    String getLatestMessage() throws RemoteException;
    void addObserver(ServerView serverView) throws RemoteException;
    String createProduct(String Id, String name, String description,double price, int stock) throws RemoteException, FileNotFoundException;
    void writeProductsToJsonFile(ArrayList<Product> products) throws RemoteException, FileNotFoundException;
    ArrayList<Receipt> showAllReceipts() throws RemoteException, FileNotFoundException;
}
