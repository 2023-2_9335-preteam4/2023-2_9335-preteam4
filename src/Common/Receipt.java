package Common;

import java.io.Serializable;

public class Receipt implements Serializable {
    private String name;
    private String diningOption;
    private String paymentOption;
    private double totalPrice;
    private String order;
    private String date;
    private String time;


    public Receipt(String name, String diningOption, String paymentOption, double totalPrice, String order, String date, String time) {
        this.name = name;
        this.diningOption = diningOption;
        this.paymentOption = paymentOption;
        this.totalPrice = totalPrice;
        this.order = order;
        this.date = date;
        this.time = time;

    }

    //setters
    public void setName(String name){this.name = name;}
    public void setDiningOption(String diningOption){this.diningOption = diningOption;}
    public void setPaymentOption(String paymentOption){this.paymentOption = paymentOption;}
    public void setTotalPrice(double totalPrice){this.totalPrice = totalPrice;}
    public void setDate(String date){this.date = date;}
    public void setTime(String time){this.time = time;}
    public void setOrder(String order){this.order = order;}

    //getters
    public String getReceiptName(){return name;}
    public String getDiningOption(){return diningOption;}
    public String getPaymentOption(){return paymentOption;}
    public double getTotalPrice(){return totalPrice;}
    public String getDate(){return date;}
    public String getTime(){return time;}
    public String getOrder(){return order;}

    public String toString(){return "\nName: " + name
            + "\nDining: " + diningOption
            + "\nPayment: "+ paymentOption
            + "\nPrice:  " + totalPrice
            + "\nDate: " + date
            + "\nTime: " + time
            + "\nOrder: " + order + "\n"; }

}
