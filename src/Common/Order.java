package Common;

import java.io.Serializable;

public class Order implements Serializable {
    private String productCode;
    private String name;
    private String desc;
    private double price;
    private int quantity;

    public Order(String productCode, String name, String desc, double price, int quantity) {
        this.productCode = productCode;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.quantity = quantity;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String toString() {
        return productCode + " " + name + " " + desc + " " + price + " " + quantity;
    }
}