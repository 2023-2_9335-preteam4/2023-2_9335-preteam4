package Common;

import java.io.Serializable;

public class Product implements Serializable {
    private String productCode;
    private String name;
    private String desc;
    private double price;
    private int stocks;

    public Product(String productCode, String name, String desc, double price, int stocks) {
        this.productCode = productCode;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.stocks = stocks;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setStocks(int stocks) {
        this.stocks = stocks;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public double getPrice() {
        return price;
    }

    public int getStocks() {
        return stocks;
    }

    public String toString() {
        return productCode + " " + name + " " + desc + " " + price + " " + stocks;
    }
}
