package Admin;

import Common.ClientHandlerInterface;
import Common.Product;
import Common.Receipt;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class AdminController extends JFrame{

    private AdminView adminView;

    private AdminModel adminModel;

    public String username;

    public static ClientHandlerInterface stub;
    public int selectedRow;

    public ArrayList<Product> products = new ArrayList<>();

    public String category, productCode , name, description, price, stock;

    public AdminController() {
        this.adminModel = new AdminModel();
        this.adminView =new AdminView(adminModel);
        add(adminView.getAdminPanel());
        setTitle("NYIRPS");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1000,700);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
        setResizable(false);

        //ActionListener to button that show panels
        adminView.getAdminConnectButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    stub = adminModel.connectAdminRMI("localhost");
                    adminModel.addObserver(adminView);
                    adminView.showAdminLogIn();
                } catch (IOException | ClassNotFoundException | NotBoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        adminView.getCreateAccountButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.setLoginErrorJLabel("");
                adminView.setInvalidPasswordJLabel("");
                adminView.setNonExistingUsernameJLabel("");
                adminView.setAdminUserNameTextField().setText("");
                adminView.setAdminPasswordField().setText("");
                adminView.showCreateAccount();
            }
        });

        adminView.getConfirmCreateButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String firstName = adminView.getNewFirstNameField();
                String lastName = adminView.getNewLastNameField();
                String createUsername = adminView.getNewAdminUsernameField();
                String createPassword = adminView.getNewAdminPasswordField();
                String email = adminView.getNewEmailTextField();
                String rePassword = adminView.getReEnterPassField();
                String isCreated;
                try {
                    isCreated = stub.createAccount(firstName, lastName, createUsername, createPassword, email, rePassword, "admin");
                    if (createUsername.isBlank()
                            || createPassword.isBlank()
                            || firstName.isBlank()
                            || lastName.isBlank()
                            || email.isBlank()
                            || rePassword.isBlank()) {
                        adminView.setInvalidEmailAdminJLabel("Please fill all the fields!");
                        adminView.setInvalidUsernameAdminJLabel("");
                        adminView.setInvalidReenterPassAdminJLabel("");
                        adminView.setAccountCreatedJLabel("");
                    } else {
                        if (isCreated.equalsIgnoreCase("email")) {
                            adminView.setInvalidEmailAdminJLabel("Email already existing!");
                            adminView.setInvalidUsernameAdminJLabel("");
                            adminView.setInvalidReenterPassAdminJLabel("");
                            adminView.setAccountCreatedJLabel("");
                        }
                        if (isCreated.equalsIgnoreCase("username")) {
                            adminView.setInvalidEmailAdminJLabel("");
                            adminView.setInvalidUsernameAdminJLabel("Username already existing!");
                            adminView.setInvalidReenterPassAdminJLabel("");
                            adminView.setAccountCreatedJLabel("");
                        }
                        if (isCreated.equalsIgnoreCase("password")) {
                            adminView.setInvalidEmailAdminJLabel("");
                            adminView.setInvalidUsernameAdminJLabel("");
                            adminView.setInvalidReenterPassAdminJLabel("Password do not match!");
                            adminView.setAccountCreatedJLabel("");
                        }
                        if (isCreated.equalsIgnoreCase("created")){
                            adminView.setAccountCreatedJLabel("Created Successfully!");
                            adminView.setInvalidEmailAdminJLabel("");
                            adminView.setInvalidUsernameAdminJLabel("");
                            adminView.setInvalidPasswordJLabel("");
                            adminView.setNewFirstNameField().setText("");
                            adminView.setNewLastNameField().setText("");
                            adminView.setNewAdminUsernameField().setText("");
                            adminView.setNewAdminPasswordField().setText("");
                            adminView.setNewEmailTextField().setText("");
                            adminView.setReEnterPassField().setText("");
                        }
                    }
                } catch (RemoteException | FileNotFoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });


        adminView.getLoginButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                username = adminView.getAdminUserNameTextField();
                String password = adminView.getAdminPasswordField();
                String isLoggedIn;

                try {
                    if (username.isBlank() || password.isBlank()) {
                        adminView.setLoginErrorJLabel("Please fill all the fields!");
                        adminView.setInvalidPasswordJLabel("");
                        adminView.setNonExistingUsernameJLabel("");
                    } else {
                        isLoggedIn = stub.validateAccount(username, password, "admin");
                        if (isLoggedIn.equalsIgnoreCase("username")) {
                            adminView.setLoginErrorJLabel("");
                            adminView.setInvalidPasswordJLabel("");
                            adminView.setNonExistingUsernameJLabel("Username not found!");
                        } else if (isLoggedIn.equalsIgnoreCase("password")) {
                            adminView.setLoginErrorJLabel("");
                            adminView.setInvalidPasswordJLabel("Incorrect Password!");
                            adminView.setNonExistingUsernameJLabel("");
                        } else {
                            adminView.setLoginErrorJLabel("");
                            adminView.setInvalidPasswordJLabel("");
                            adminView.setNonExistingUsernameJLabel("");
                            adminView.setAdminUserNameTextField().setText("");
                            adminView.setAdminPasswordField().setText("");
                            setTitle(username);
                            adminView.showDatabase();
                        }
                    }
                } catch (Exception a) {
                    a.printStackTrace();
                }
            }
        });

        adminView.getShowAllProductsButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    products = stub.showAllProductsJSON();
                    adminView.allTable(products);
                    adminView.showAllProducts();
                } catch (RemoteException ex) {
                    throw new RuntimeException(ex);
                } catch (FileNotFoundException ex) {
                    throw new RuntimeException(ex);
                }


            }
        });

        adminView.getShowAllProductsFromAddButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.setIdTextField().setText("");
                adminView.setNameTextField().setText("");
                adminView.setDescriptionTextField().setText("");
                adminView.setPriceTextField().setText("");
                adminView.setStockTextField().setText("");
                adminView.setProductAddedJLabel("");
                adminView.allTable(products);
                adminView.showAllProducts();
            }
        });

        adminView.getAddProductsButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.showAddProducts();
            }
        });
        adminView.getAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                productCode = adminView.getIdTextField();
                name = adminView.getNameTextField();
                description = adminView.getDescriptionTextField();
                price = adminView.getPriceTextField();
                stock = adminView.getStockTextField();

                try {
                    if (productCode.isBlank() || name.isBlank() || description.isBlank()
                            || price.isBlank() || stock.isBlank()) {
                        adminView.setProductAddedJLabel("Please fill all the fields!");
                        throw new RuntimeException("Error");
                    }
                    stub.createProduct(productCode, name, description, Double.parseDouble(price), Integer.parseInt(stock));
                    adminView.setProductAddedJLabel("Added Successfully!");
                    adminView.setIdTextField().setText("");
                    adminView.setNameTextField().setText("");
                    adminView.setDescriptionTextField().setText("");
                    adminView.setPriceTextField().setText("");
                    adminView.setStockTextField().setText("");
                } catch (RemoteException | FileNotFoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        adminView.getAdminHistoryButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Receipt> history = stub.showAllReceipts();
                    StringBuilder historyText = new StringBuilder();
                    for (Receipt receipt : history) {
                        historyText.append(receipt.toString()).append("\n");
                    }
                    adminView.getHistoryTextArea().setText(historyText.toString());
                    adminView.showAdminHistory();
                } catch (RemoteException | FileNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });

        adminView.getDeleteProductsButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    products = stub.showAllProductsJSON();
                    adminView.deleteTable(products);
                    adminView.showDeleteProducts();
                } catch (RemoteException ex) {
                    throw new RuntimeException(ex);
                } catch (FileNotFoundException ex) {
                    throw new RuntimeException(ex);
                }

            }
        });

        adminView.getDeleteButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                for (int i = 0; i < products.size(); i++) {
                    if (i == selectedRow - 1) {
                        products.remove(products.get(i));
                        adminView.setProductDeletedJLabel("Deleted Successfully!");
                        try {
                            stub.writeProductsToJsonFile(products);
                        } catch (RemoteException | FileNotFoundException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                }


            }
        });

        adminView.getDeleteTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                adminView.setProductDeletedJLabel("");
                selectedRow = adminView.getDeleteTable().getSelectedRow();
            }
        });

        //ActionListener to back button to each panel
        adminView.getShowAllBackButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.showDatabase();
            }
        });

        adminView.getCreateAccountBackButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminView.setAccountCreatedJLabel("");
                adminView.setInvalidEmailAdminJLabel("");
                adminView.setInvalidUsernameAdminJLabel("");
                adminView.setInvalidPasswordJLabel("");
                adminView.setNewFirstNameField().setText("");
                adminView.setNewLastNameField().setText("");
                adminView.setNewAdminUsernameField().setText("");
                adminView.setNewAdminPasswordField().setText("");
                adminView.setNewEmailTextField().setText("");
                adminView.setReEnterPassField().setText("");
                adminView.showAdminLogIn();
            }
        });

        adminView.getAddProductsBackButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.setIdTextField().setText("");
                adminView.setNameTextField().setText("");
                adminView.setDescriptionTextField().setText("");
                adminView.setPriceTextField().setText("");
                adminView.setStockTextField().setText("");
                adminView.setProductAddedJLabel("");
                adminView.showDatabase();
            }
        });

        adminView.getAdminHistoryBackButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.showDatabase();
            }
        });

        adminView.getDeleteBackButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.setProductDeletedJLabel("");
                adminView.showDatabase();
            }
        });

        //ActionListener for back button
        adminView.getDatabaseLogoutButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                adminView.showAdminLogIn();
            }
        });
    }
}