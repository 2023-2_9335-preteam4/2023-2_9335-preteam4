package Admin;

import Common.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.ArrayList;

public class AdminView extends JFrame {

    /**
     * PANELS
     */
    private JPanel adminConnect;
    private JPanel adminPanel;
    private JPanel adminLogin;
    private JPanel createAccountPanel;
    private JPanel databasePanel;
    private JPanel allProductsPanel;
    private JPanel adminHistoryPanel;
    private JPanel addProductsPanel;
    private JPanel deleteProductsPanel;

    /**
     * TEXTFIELDS
     */
    private JTextField adminUserNameTextField;
    private JTextField idTextField;
    private JTextField nameTextField;
    private JTextField descriptionTextField;
    private JTextField priceTextField;
    private JTextField stockTextField;
    private JTextField newEmailTextField;
    private JTextField newAdminUsernameField;
    private JTextField newFirstNameField;
    private JTextField newLastNameField;
    private JPasswordField newAdminPasswordField;
    private JPasswordField reEnterPassField;
    private JPasswordField adminPasswordField;
    private JTextField categoryTextField;

    /**
     * BUTTONS
     */
    private JButton adminConnectButton;
    private JButton loginButton;
    private JButton showAllProductsButton;
    private JButton addProductsButton;
    private JButton addButton;
    private JButton adminHistoryButton;
    private JButton deleteProductsButton;
    private JButton showAllBackButton;
    private JButton addProductsBackButton;
    private JButton adminHistoryBackButton;
    private JButton deleteBackButton;
    private JButton deleteButton;
    private JButton databaseLogoutButton;
    private JButton createAccountButton;
    private JButton confirmCreateButton;
    private JButton showAllProductsFromAddButton;
    private JButton createAccountBackButton;

    /**
     * LABELS
     */
    private JLabel adminWelcome;
    private JLabel firstNameJLabel;
    private JLabel lastNameJLabel;
    private JLabel reenterPasswordJLabel;
    private JLabel emailJLabel;
    private JLabel deleteMessage;
    private JLabel invalidPasswordJLabel;
    private JLabel existingUsernameJLabel;
    private JLabel loginErrorJLabel;
    private JLabel invalidEmailAdminJLabel;
    private JLabel invalidUsernameAdminJLabel;
    private JLabel invalidReenterPassAdminJLabel;
    private JLabel accountCreatedJLabel;
    private JLabel productAddedJLabel;
    private JLabel productDeletedJLabel;

    /**
     * SCROLLPANE
     */
    private JScrollPane allScrollPane;
    private JScrollPane adminHistoryScrollPane;
    /**
     * TABLES & TEXTAREA
     */
    private JTable deleteTable;
    private JTable allTable;
    private JTextArea historyTextArea;
    private AdminModel adminModel;

    public AdminView(AdminModel adminModel) {
        this.adminModel = adminModel;
    }

    public JPanel getAdminPanel() {
        return adminPanel;
    }

    /**
     * adminConnect Jpanel
     */
    public JButton getAdminConnectButton() {
        return adminConnectButton;
    }

    public void showAdminConnect() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "adminconnect");
    }

    /**
     * adminLogin Jpanel
     */
    public JButton getLoginButton() {
        return loginButton;
    }

    public JButton getCreateAccountButton() {
        return createAccountButton;
    }

    public String getAdminUserNameTextField() {
        return adminUserNameTextField.getText();
    }

    public String getAdminPasswordField() {
        return adminPasswordField.getText();
    }

    public JTextField setAdminUserNameTextField() {
        return adminUserNameTextField;
    }

    public JPasswordField setAdminPasswordField() {
        return adminPasswordField;
    }

    public void setLoginErrorJLabel(String message){
        loginErrorJLabel.setText(message);
    }
    public void setInvalidPasswordJLabel(String message){
        invalidPasswordJLabel.setText(message);
    }
    public void setNonExistingUsernameJLabel(String message){
        existingUsernameJLabel.setText(message);
    }

    public void showAdminLogIn() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "adminlogin");
    }


    /**
     * createAccount Jpanel
     */

    public String getNewEmailTextField() {
        return newEmailTextField.getText();
    }
    public String getNewAdminUsernameField() {
        return newAdminUsernameField.getText();
    }
    public String getNewFirstNameField() {
        return newFirstNameField.getText();
    }
    public String getNewLastNameField() {
        return newLastNameField.getText();
    }
    public String getNewAdminPasswordField() {
        return newAdminPasswordField.getText();
    }
    public String getReEnterPassField() {
        return reEnterPassField.getText();
    }
    public JTextField setNewEmailTextField(){return newEmailTextField;}
    public JTextField setNewAdminUsernameField() {
        return newAdminUsernameField;
    }
    public JTextField setNewFirstNameField() {
        return newFirstNameField;
    }
    public JTextField setNewLastNameField() {
        return newLastNameField;
    }
    public JPasswordField setNewAdminPasswordField() {
        return newAdminPasswordField;
    }
    public JPasswordField setReEnterPassField() {
        return reEnterPassField;
    }

    public JButton getConfirmCreateButton() {
        return confirmCreateButton;
    }
    public void setAccountCreatedJLabel(String message){
        accountCreatedJLabel.setText(message);
    }
    public void setInvalidReenterPassAdminJLabel(String message){
        invalidReenterPassAdminJLabel.setText(message);
    }
    public void setInvalidUsernameAdminJLabel(String message){
        invalidUsernameAdminJLabel.setText(message);
    }
    public void setInvalidEmailAdminJLabel(String message){
        invalidEmailAdminJLabel.setText(message);
    }

    public void showCreateAccount() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "createaccount");
    }

    public JButton getCreateAccountBackButton() {
        return createAccountBackButton;
    }


    /**
     * database Jpanel
     */
    public JButton getShowAllProductsButton() {
        return showAllProductsButton;
    }

    public JButton getAddProductsButton() {
        return addProductsButton;
    }

    public JButton getAdminHistoryButton() {
        return adminHistoryButton;
    }

    public JButton getDeleteProductsButton() {
        return deleteProductsButton;
    }

    public JButton getDatabaseLogoutButton() {
        return databaseLogoutButton;
    }

    public void showDatabase() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "database");
    }


    /**
     * allProducts Jpanel
     */
    public JTable getAllTable() {
        return allTable;
    }

    public void allTable(ArrayList<Product> products) {
        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCKS"};
        DefaultTableModel model = new DefaultTableModel(products.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getAllTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getAllTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < products.size(); i++) {
            getAllTable().setValueAt(products.get(i).getName(), i + 1, 0);
            getAllTable().setValueAt(products.get(i).getDesc(), i + 1, 1);
            getAllTable().setValueAt(products.get(i).getPrice(), i + 1, 2);
            getAllTable().setValueAt(products.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel allColumnModel = getAllTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        allColumnModel.getColumn(0).setCellRenderer(center);
        allColumnModel.getColumn(0).setPreferredWidth(100);
        allColumnModel.getColumn(1).setCellRenderer(center);
        allColumnModel.getColumn(1).setPreferredWidth(200);
        allColumnModel.getColumn(2).setCellRenderer(center);
        allColumnModel.getColumn(3).setCellRenderer(center);
        getAllTable().setColumnModel(allColumnModel);
    }

    public JButton getShowAllBackButton() {
        return showAllBackButton;
    }

    public void showAllProducts() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "allproducts");
    }


    /**
     * addProducts Jpanel
     */
    public JButton getAddButton() {
        return addButton;
    }

    public JButton getShowAllProductsFromAddButton() {
        return showAllProductsFromAddButton;
    }

    public JButton getAddProductsBackButton() {
        return addProductsBackButton;
    }

    public String getIdTextField() {
        return idTextField.getText();
    }

    public String getNameTextField() {
        return nameTextField.getText();
    }

    public String getDescriptionTextField() {
        return descriptionTextField.getText();
    }

    public String getPriceTextField() {
        return priceTextField.getText();
    }

    public String getStockTextField() {
        return stockTextField.getText();
    }

    public JTextField setIdTextField() {
        return idTextField;
    }

    public JTextField setNameTextField() {
        return nameTextField;
    }

    public JTextField setDescriptionTextField() {
        return descriptionTextField;
    }

    public JTextField setPriceTextField() {
        return priceTextField;
    }

    public JTextField setStockTextField() {
        return stockTextField;
    }

    public String getCategoryTextField(){return categoryTextField.getText();}
    public void setProductAddedJLabel(String message){
        productAddedJLabel.setText(message);
    }

    public void showAddProducts() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "addproducts");
    }


    /**
     * adminHistory Jpanel
     */
    public JTextArea getHistoryTextArea() {
        return historyTextArea;
    }
    public JButton getAdminHistoryBackButton() {
        return adminHistoryBackButton;
    }
    public JScrollPane getAdminHistoryScrollPane() {
        return adminHistoryScrollPane;
    }
    public void showAdminHistory() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "adminhistory");
    }


    /**
     * deleteProducts Jpanel
     */
    public JTable getDeleteTable() {
        return deleteTable;
    }

    public void deleteTable(ArrayList<Product> Products) {
        String[] header = {"NAME", "DESCRIPTION", "PRICE", "STOCKS"};
        DefaultTableModel model = new DefaultTableModel(Products.size() + 1, header.length) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        getDeleteTable().setModel(model);
        for (int i = 0; i < header.length; i++) {
            getDeleteTable().setValueAt(header[i], 0, i);
        }
        for (int i = 0; i < Products.size(); i++) {
            getDeleteTable().setValueAt(Products.get(i).getName(), i + 1, 0);
            getDeleteTable().setValueAt(Products.get(i).getDesc(), i + 1, 1);
            getDeleteTable().setValueAt(Products.get(i).getPrice(), i + 1, 2);
            getDeleteTable().setValueAt(Products.get(i).getStocks(), i + 1, 3);
        }
        TableColumnModel deleteColumnModel = getDeleteTable().getColumnModel();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(JLabel.CENTER);
        deleteColumnModel.getColumn(0).setCellRenderer(center);
        deleteColumnModel.getColumn(0).setPreferredWidth(100);
        deleteColumnModel.getColumn(1).setCellRenderer(center);
        deleteColumnModel.getColumn(1).setPreferredWidth(200);
        deleteColumnModel.getColumn(2).setCellRenderer(center);
        deleteColumnModel.getColumn(3).setCellRenderer(center);
        getDeleteTable().setColumnModel(deleteColumnModel);
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getDeleteBackButton() {
        return deleteBackButton;
    }
    public void setProductDeletedJLabel(String message){
        productDeletedJLabel.setText(message);
    }

    public void showDeleteProducts() {
        CardLayout cardLayout = (CardLayout) adminPanel.getLayout();
        cardLayout.show(adminPanel, "deleteproducts");
    }

//    public void clearTextFields() {
//        getCategoryTextField().setText("");
//    }
}