package Admin;

import Common.ClientHandlerInterface;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class AdminModel {
    private static Registry registry;
    public static ClientHandlerInterface stub;
    private AdminView adminView;

    public ClientHandlerInterface connectAdminRMI(String serverIPAddress) throws IOException, ClassNotFoundException, NotBoundException {
        try {
            registry = LocateRegistry.getRegistry(serverIPAddress, Registry.REGISTRY_PORT);
            Remote remoteObj = registry.lookup("ServerModel");
            if (remoteObj instanceof ClientHandlerInterface) {
                stub = (ClientHandlerInterface) remoteObj;
                return stub;
            } else {
                throw new RuntimeException("Remote Object does not implement ClientHandlerInterface");
            }
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to initialize RMI connection", e);
        }
    }

    //add product errors

    public void checkAddProductErrors(int index, String category,String productCode, String name, String description, String price, String stock ){
        if(category.isEmpty()){
            // no error label // adminView.setAdd
        }

    }

    public void addObserver(AdminView adminView) {
        this.adminView = adminView;
    }
}
